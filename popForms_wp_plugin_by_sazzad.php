<?php
/*
Plugin Name: popForms_wp_plugin_by_sazzad
Plugin URI: https://www.github.io/sazzad24x7/popForms_wp_plugin_by_sazzad
Description: Just another popForms_wp_plugin_by_sazzad. Simple but flexible.
Author: Syed Sazzad Hossain
Author URI: https://www.sazzad24x7.tk
Text Domain: popForms_wp_plugin_by_sazzad
Domain Path: /languages/
Version: 1.0.0
*/